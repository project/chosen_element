This is a helper module that gives you the ability to create Chosen select box
elements. It uses hook_elements to create a "chosen_select" element type. So
for select boxes that you want to use Chosen, set its type to "chosen_select"
instead of "select".

This module requires the Libraries API module, and for you to have the Chosen
library files on your system. Put chosen.jquery.js and chosen.css inside a
folder located here: sites/all/libraries/chosen/

This module is sponsored by AllPlayers.com.
